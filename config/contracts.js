module.exports = {
  // default applies to all environments
  default: {
    // Blockchain node to deploy the contracts
    deployment: {
      host: "localhost", // Host of the blockchain node
      port: 8545, // Port of the blockchain node
      type: "rpc" // Type of connection (ws or rpc),
      // Accounts to use instead of the default account to populate your wallet
      ,accounts: [
        {
          mnemonic: "example exile argue silk regular smile grass bomb merge arm assist farm",
          addressIndex: "0", // Optionnal. The index to start getting the address
          numAddresses: "4", // Optionnal. The number of addresses to get
          hdpath: "m/44'/60'/0'/0/" // Optionnal. HD derivation path
          , balance: "5 ether"  
        }
      ]
    },
    // order of connections the dapp should connect to
    dappConnection: [
      "$WEB3",  // uses pre existing web3 object if available (e.g in Mist)
      "ws://localhost:8546",
      "http://localhost:8545"
    ],
    gas: "auto",
    contracts: {
      // example:
      Burner: {
       args: [ "1000000000000000000000000" ]
      },
      BurnWallet: {
        args: [ "$Burner"]
      }
    }
  }
};
