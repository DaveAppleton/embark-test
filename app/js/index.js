import EmbarkJS from 'Embark/EmbarkJS';

// import your contracts
// e.g if you have a contract named SimpleStorage:
//import SimpleStorage from 'Embark/contracts/SimpleStorage';
import $ from 'jquery';
import Burner from 'Embark/contracts/Burner';

$(document).ready(function() {

  web3.eth.getAccounts(function(err, accounts) {
    $('#queryBalance input').val(accounts[0]);
    $('#transfer .address').val(accounts[1]);
    console.log(web3.eth.getAccounts());
    console.log(accounts[1]);
    
  });

  $('#queryBalance button').click(function() {
    var address = $('#queryBalance input').val();
    Burner.methods.balanceOf(address).call().then(function(balance) {
      $('#queryBalance .result').html(balance);
    });
  });

  $('#transfer button').click(function() {
    var address = $('#transfer .address').val();
    var num = $('#transfer .num').val();

    Burner.methods.transfer(address, num).send().then(function() {
      $('#transfer .result').html('Done!');
    });;
  });

});