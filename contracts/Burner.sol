pragma solidity ^0.4.24;

contract Burner {
    // used for the adjusted calculations
    
    mapping (address=>uint) public balances;
    uint public totalSupply;
    uint public allocation;

    constructor(uint supply) public {
        balances[msg.sender] = supply;
        totalSupply = supply;
    }

    function viewShare(address from) public view returns (uint) {
        return balances[from] * allocation / totalSupply;
    }

    function setAllocation(uint alloc) public {
        allocation = alloc;
    }

    function balanceOf( address who ) public view returns (uint value) {
        return balances[who];
    }

    function transfer(address to, uint amount) public returns (bool) {
        require(amount <= balances[msg.sender],"insufficient balance");
        balances[msg.sender] -= amount;
        balances[to] += amount;
        return true;
    }

    function burn(uint amount) public returns (bool) {
        require(amount <= balances[msg.sender],"insufficient balance");
        balances[msg.sender] -= amount;
        totalSupply -= amount;
    }

}
 