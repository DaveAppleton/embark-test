pragma solidity ^0.4.24;

import "./Burner.sol";

contract BurnWallet {

    Burner token;

    constructor(Burner tkn) public {
        token = tkn;
    }

    function adjustAllocation(uint amount) public {
        uint ts = token.totalSupply();
        uint tb = token.balanceOf(address(this));
        uint revisedAmount = amount * ts / (ts - tb);
        token.setAllocation(revisedAmount);
    }
}